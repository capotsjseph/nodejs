module.exports = {
  login: (req, res) => {
    const {
      username,
      password
    } = req.body || req.param

    //validator
    if (_.isUndefined(username)) return res.badRequest("Invalid Credentials.");
    if (_.isUndefined(password)) return res.badRequest("Invalid Credentials.");

    let tasks = {
      'login': (next) => {
        Users.findOne({
            username,
            password
          })
          .exec((err, data) => {
            if (err)
              return next(err)
            if (_.isEmpty(data)) {
              return next({
                err: 404
              })
            }
            return next(null, data)
          })
      }
    }
    async.auto(tasks, (err, results) => {
      return err ?
        err.err === 404 ? res.notFound() :
        res.serverError(err) :
        res.json({
          err: null,
          msg: 'Succesfull.',
          data: results.login
        });
    })
  }
}
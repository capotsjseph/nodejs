module.exports = SocketService;

function SocketService(request) {

  Object.defineProperty(this, 'blast', {
    value: (events, values, done) => {
      if (_.isUndefined(events)) return res.badRequest("Invalid Credentials.");
      if (_.isUndefined(values)) return res.badRequest("Invalid Credentials.");
      const tasks = {
        blast: (next) => {
          sails.socket.blast(event, values)
          return next()
        }
      }
      async.auto(tasks, done)
    }
  })

  Object.defineProperty(this, 'broadcast', {
    value: (room, values, event, done) => {
        if (_.isUndefined(events)) return res.badRequest("Required.");
        if (_.isUndefined(values)) return res.badRequest("Required.");
        if (_.isUndefined(room)) return res.badRequest("Required.");
      const tasks = {
        broadcast:(next) => {
          if (event) {
            sails.sockets.broadcast(room, event, values);
            console.info(`Broadcast message ${room}[${event}] : ${values}`);
            return next();
          }
          sails.sockets.broadcast(room, values);
          console.info(`Broadcast message ${room} : ${values}`);
          return next();
        }
      };

      async.auto(tasks, done);
    }
  });
}
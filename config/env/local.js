
module.exports = {

    datastores: {
        default: {
            adapter: 'sails-mysql',
            database: 'sample',
            host: 'localhost',
            user: 'root',
            password: '',
            port: 3306,
            connectionTimeout: 30000
        }
    },
    models: {
        migrate: 'safe'
    },
    blueprints: {
        shorcuts: false
    },
    port: 8003
}